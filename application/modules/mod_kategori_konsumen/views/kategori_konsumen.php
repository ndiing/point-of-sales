<table id="datagrid_kategori_konsumen" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_kategori_konsumen" url="<?php echo base_url('mod_kategori_konsumen/baca_kategori_konsumen'); ?>">
	<thead>
		<tr>
			<th field="id_kategori_konsumen">Kode Kategori</th>
			<th field="nama_kategori_konsumen">Nama Kategori</th>
			<th field="keterangan">Keterangan</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_kategori_konsumen" class="easyui-dialog" buttons="#buttons_kategori_konsumen" closed="true" modal="true" style="height:175px;width:400px;">
	<form id="form_kategori_konsumen" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr style="display:none;">
					<th><label>id_kategori_konsumen</label></th>
					<th><input type="hidden" name="id_kategori_konsumen"></th>
				</tr>
				<tr>
					<th><label>Nama Kategori</label></th>
					<th><input type="text" name="nama_kategori_konsumen"></th>
				</tr>
				<tr>
					<th><label>Keterangan</label></th>
					<th><input type="text" name="keterangan"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_kategori_konsumen">
	<a href="javascript:void(0);" plain="true" onclick="tambah_kategori_konsumen();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_kategori_konsumen();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_kategori_konsumen();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_kategori_konsumen">
	<a href="javascript:void(0);" onclick="simpan_kategori_konsumen();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_kategori_konsumen').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>