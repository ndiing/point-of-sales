<?php

class Mod_kategori_konsumen extends MX_Controller
{
	public $table = 'kategori_konsumen';
	public $primary = 'id_kategori_konsumen';
	public function index() 
	{
		$this->load->view('kategori_konsumen');
	}
	public function validasi_kategori_konsumen()
	{
		$config = array(
			array(
				'field' => 'id_kategori_konsumen',
				'label' => 'id_kategori_konsumen',
				'rules' => '',
				),
			array(
				'field' => 'nama_kategori_konsumen',
				'label' => 'nama_kategori_konsumen',
				'rules' => 'required',
				),
			array(
				'field' => 'keterangan',
				'label' => 'keterangan',
				'rules' => 'required',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_kategori_konsumen() 
	{
		if ($this->validasi_kategori_konsumen() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_kategori_konsumen' => $_POST['id_kategori_konsumen'],
			'nama_kategori_konsumen' => $_POST['nama_kategori_konsumen'],
			'keterangan' => $_POST['keterangan'],		
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_kategori_konsumen() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_kategori_konsumen() 
	{
		if ($this->validasi_kategori_konsumen() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_kategori_konsumen' => $_POST['id_kategori_konsumen'],
			'nama_kategori_konsumen' => $_POST['nama_kategori_konsumen'],						
			'keterangan' => $_POST['keterangan'],
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_kategori_konsumen() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>mysql_error()));
		}
	}
}