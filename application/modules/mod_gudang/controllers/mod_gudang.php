<?php

class Mod_gudang extends MX_Controller
{
	public $table = 'gudang';
	public $primary = 'kode';
	public function index() 
	{
		$this->load->view('gudang');
	}
	public function baca_stok_terakhir($kode)
	{
		$query = $this->db->where('kode', $kode)->get($this->table)->row()->stok;
		return $query;
	}
	public function kurang_stok_gudang($kode, $jumlah)
	{
		$data = array(
			'stok' => $this->baca_stok_terakhir($kode) - $jumlah
		);

		$query = $this->db
		->where('kode', $kode)
		->update($this->table, $data);

		if ($query) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function tambah_stok_gudang($kode, $jumlah)
	{
		$data = array(
			'stok' => $this->baca_stok_terakhir($kode) + $jumlah
		);

		$query = $this->db
		->where('kode', $kode)
		->update($this->table, $data);

		if ($query) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function validasi_gudang()
	{
		$config = array(
			array(
				'field' => 'kode',
				'label' => 'kode',
				'rules' => ''
				),
			array(
				'field' => 'nama_barang',
				'label' => 'nama_barang',
				'rules' => ''
				),
			array(
				'field' => 'jenis',
				'label' => 'jenis',
				'rules' => ''
				),
			array(
				'field' => 'merk',
				'label' => 'merk',
				'rules' => ''
				),
			array(
				'field' => 'satuan',
				'label' => 'satuan',
				'rules' => ''
				),
			array(
				'field' => 'modal',
				'label' => 'modal',
				'rules' => ''
				),
			array(
				'field' => 'margin',
				'label' => 'margin',
				'rules' => ''
				),
			array(
				'field' => 'stok',
				'label' => 'stok',
				'rules' => ''
				),
			array(
				'field' => 'waktu_catat',
				'label' => 'waktu_catat',
				'rules' => ''
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_gudang() 
	{
		if ($this->validasi_gudang() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'kode' => $_POST['kode'],
			'nama_barang' => $_POST['nama_barang'],
			'jenis' => $_POST['jenis'],
			'merk' => $_POST['merk'],
			'satuan' => $_POST['satuan'],
			'modal' => $_POST['modal'],
			'margin' => $_POST['margin'],
			'stok' => $_POST['stok'],
			// 'waktu_catat	' => $_POST['waktu_catat'],						
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_gudang() 
	{
		$result = array();
		$query = $this->db
		->join('jenis', 'id_jenis = jenis')
		->join('merk', 'id_merk = merk')
		->join('satuan', 'id_satuan = satuan')
		->get($this->table);
		foreach ($query->result() as $value) {
			$value->v_nama_barang = $value->nama_barang.'/'.$value->nama_satuan;
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_gudang() 
	{
		if ($this->validasi_gudang() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'kode' => $_POST['kode'],
			'nama_barang' => $_POST['nama_barang'],
			'jenis' => $_POST['jenis'],
			'merk' => $_POST['merk'],
			'satuan' => $_POST['satuan'],
			'modal' => $_POST['modal'],
			'margin' => $_POST['margin'],
			'stok' => $_POST['stok'],
			// 'waktu_catat' => $_POST['waktu_catat'],					
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_gudang() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}