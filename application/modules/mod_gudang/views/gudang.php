<div class="easyui-tabs" fit="true" border="false">
	<div title="Gudang">
		<table id="datagrid_gudang" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_gudang" url="<?php echo base_url('mod_gudang/baca_gudang'); ?>">
			<thead>
				<tr>
					<th field="kode" width="50">Kode</th>
					<th field="nama_barang" width="100">Nama Barang</th>
					<th field="nama_jenis" width="100">Jenis</th>
					<th field="nama_merk" width="100">Merk</th>
					<th field="nama_satuan" width="50">Satuan</th>
					<th field="modal" width="50">Modal</th>
					<th field="margin" width="50">Margin</th>
					<th field="stok" width="50">Stok</th>
					<th field="waktu_catat" width="100">Waktu Catat</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<div id="dialog_gudang" class="easyui-dialog" buttons="#buttons_gudang" closed="true" modal="true" style="height:315px;width:400px;">
			<form id="form_gudang" method="post" class="easyui-form">
				<table>
					<tbody>
						<tr style="display:none;">
							<td><label>Kode</label></td>
							<td><input type="hidden" name="kode"></td>
						</tr>
						<tr>
							<td><label>Nama Barang</label></td>
							<td><input type="text" name="nama_barang"></td>
						</tr>
						<tr>
							<td><label>Jenis</label></td>
							<td><input type="text" name="jenis" class="easyui-combobox" valueField="id_jenis" textField="nama_jenis" url="mod_jenis/baca_jenis" style="width:296px;"></td>
						</tr>
						<tr>
							<td><label>Merk</label></td>
							<td><input type="text" name="merk" class="easyui-combobox" valueField="id_merk" textField="nama_merk" url="mod_merk/baca_merk" style="width:296px;"></td>
						</tr>
						<tr>
							<td><label>Satuan</label></td>
							<td><input type="text" name="satuan" class="easyui-combobox" valueField="id_satuan" textField="nama_satuan" url="mod_satuan/baca_satuan" style="width:296px;"></td>
						</tr>
						<tr>
							<td><label>Modal</label></td>
							<td><input type="text" name="modal" class="easyui-numberbox"></td>
						</tr>
						<tr>
							<td><label>Margin</label></td>
							<td><input type="text" name="margin" class="easyui-numberbox"></td>
						</tr>
						<tr>
							<td><label>Stok</label></td>
							<td><input type="text" name="stok" class="easyui-numberbox"></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div id="toolbar_gudang">
			<a href="javascript:void(0);" plain="true" onclick="tambah_gudang();" class="easyui-linkbutton">Tambah</a>
			<a href="javascript:void(0);" plain="true" onclick="edit_gudang();" class="easyui-linkbutton">Edit</a>
			<a href="javascript:void(0);" plain="true" onclick="hapus_gudang();" class="easyui-linkbutton">Hapus</a>
		</div>
		<div id="buttons_gudang">
			<a href="javascript:void(0);" onclick="simpan_gudang();" class="easyui-linkbutton">Simpan</a>
			<a href="javascript:void(0);" onclick="$('#dialog_gudang').dialog('close');" class="easyui-linkbutton">Batal</a>
		</div>
	</div>
	<div title="Jenis" href="mod_jenis" closable="true"></div>
	<div title="Merk" href="mod_merk" closable="true"></div>
	<div title="Satuan" href="mod_satuan" closable="true"></div>
</div>