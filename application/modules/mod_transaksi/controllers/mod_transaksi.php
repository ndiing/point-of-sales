<?php

class Mod_transaksi extends MX_Controller
{
	public $table = 'transaksi';
	public $primary = 'id_transaksi';
	public function index() 
	{
		$this->load->view('transaksi');
	}
	public function validasi_transaksi()
	{
		$config = array(
			array(
				'field' => 'id_transaksi',
				'label' => 'id_transaksi',
				'rules' => ''
				),
			array(
				'field' => 'keranjang_id',
				'label' => 'keranjang_id',
				'rules' => ''
				),
			array(
				'field' => 'jumlah_pembayaran',
				'label' => 'jumlah_pembayaran',
				'rules' => 'required'
				),
			array(
				'field' => 'jumlah_kembalian',
				'label' => 'jumlah_kembalian',
				'rules' => ''
				),
			array(
				'field' => 'konsumen_id',
				'label' => 'konsumen_id',
				'rules' => 'required'
				),
			array(
				'field' => 'jenis_transaksi',
				'label' => 'jenis_transaksi',
				'rules' => 'required'
				),
			array(
				'field' => 'pengirim',
				'label' => 'pengirim',
				'rules' => ''
				),
			array(
				'field' => 'pengguna_id',
				'label' => 'pengguna_id',
				'rules' => ''
				),
			array(
				'field' => 'waktu_transaksi',
				'label' => 'waktu_transaksi',
				'rules' => ''
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_transaksi() 
	{
		if ($this->validasi_transaksi() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		if (intval($_POST['jumlah_pembayaran']) < intval($_POST['jumlah_total'])) {
			echo json_encode(array('msg'=>'Jumlah pembayaran tidak mencukupi untuk melakukan transaksi ini.'));
			return FALSE;
		}

		$data = array(
			// 'id_transaksi' => $_POST['id_transaksi'],
			'keranjang_id' => modules::run('mod_keranjang/ref_keranjang'),
			'jumlah_pembayaran' => $_POST['jumlah_pembayaran'],
			'jumlah_kembalian' => intval($_POST['jumlah_pembayaran']) - intval($_POST['jumlah_total']),
			'konsumen_id' => $_POST['konsumen_id'],
			'jenis_transaksi' => $_POST['jenis_transaksi'],
			'pengirim' => $_POST['pengirim'],
			// 'pengguna_id' => $_POST['pengguna_id'],
			// 'waktu_transaksi	' => $_POST['waktu_transaksi	'],	
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			modules::run('mod_keranjang/buat_keranjang');
			echo json_encode(array('success'=>TRUE, 'msg'=>'Transaksi berhasil, jumlah kembalian Rp'.number_format($data['jumlah_kembalian'],0,'','.')));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_transaksi() 
	{
		$result = array();
		$query = $this->db
		->join('konsumen', 'id_konsumen = konsumen_id')
		->join('jenis_transaksi', 'id_jenis_transaksi = jenis_transaksi')
		->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_transaksi() 
	{
		if ($this->validasi_transaksi() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_transaksi' => $_POST['id_transaksi'],
			'keranjang_id' => $_POST['keranjang_id'],
			'jumlah_pembayaran' => $_POST['jumlah_pembayaran'],
			'jumlah_kembalian' => $_POST['jumlah_kembalian'],
			'komsumen_id' => $_POST['komsumen_id'],
			'jenis_transaksi' => $_POST['jenis_transaksi'],
			'pengirim' => $_POST['pengirim'],
			'pengguna_id' => $_POST['pengguna_id'],
			// 'waktu_transaksi' => $_POST['waktu_transaksi'],					
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_transaksi() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}