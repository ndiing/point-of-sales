<div class="easyui-layout" fit="true">
	<div region="center" border="false">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false">
				<table id="datagrid_keranjang" title="Transaksi" iconCls="icon-demo" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_keranjang" url="<?php echo base_url('mod_keranjang/baca_keranjang'); ?>">
					<thead>
						<tr>
							<th field="kode_barang" width="50">Kode</th>
							<th field="nama_barang" width="100">Nama Barang</th>
							<th field="nama_jenis" width="100">Jenis</th>
							<th field="nama_satuan" width="100">satuan</th>
							<th field="nama_merk" width="100">Merk</th>
							<th field="harga" width="75">Harga</th>
							<th field="jumlah" width="75">Jumlah</th>
							<th field="sub_total" width="75">Sub Total</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
	<div region="east" split="true" border="false" class="align-center graylight" style="width:300px;padding:10px;">
		<div class="easyui-panel gray" noheader="true" border="false" style="width:272px;padding:10px 10px 10px 10px;">
			<div class="row total">
				<div class="col2 total-sign">Rp</div>
				<div class="col10">
					<input id="cal-total" class="easyui-numberbox total-price" value="0" precision="2" groupSeparator="." decimalSeparator=",">
				</div>
			</div>
			<form id="form_keranjang" method="post" class="cart">
				<div class="row">
					<div class="col10">
						<input type="text" class="easyui-combobox" name="kode_barang" cls="cart-field" valueField="kode" textField="v_nama_barang" url="mod_gudang/baca_gudang" style="width:209px;">
					</div>
					<div class="col2">
						<input type="text" class="col12 cart-field" name="jumlah">
					</div>
				</div>
			</form>
			<a href="javascript:void(0);" id="simpan_keranjang" onclick="simpan_keranjang()" class="cart-button">Tambahkan Barang</a>
		</div>
		<form method="post" class="cart" id="form_transaksi">
			<input type="hidden" name="jumlah_total" id="cal-total-value">
			<div class="easyui-panel gray" noheader="true" border="false" style="width:272px;padding:0px 10px 10px 10px;margin-top:10px;">
				<div class="row">
					<div class="col3">
						<label class="cart-label">Konsumen</label>
					</div>
					<div class="col9">
						<input class="easyui-combobox" cls="cart-field" name="konsumen_id" value="1" valueField="id_konsumen" textField="nama_konsumen" url="mod_konsumen/baca_konsumen" style="width:188px;"></input>
					</div>
				</div>
				<div class="row">
					<div class="col3">
						<label class="cart-label">Transaksi</label>
					</div>
					<div class="col9">
						<input class="easyui-combobox" cls="cart-field" name="jenis_transaksi" value="1" valueField="id_jenis_transaksi" textField="nama_jenis_transaksi" url="mod_jenis_transaksi/baca_jenis_transaksi" style="width:188px;"></input>
					</div>
				</div>
				<div class="row">
					<div class="col3">
						<label class="cart-label">Pengirim</label>
					</div>
					<div class="col9">
						<input class="easyui-combobox" cls="cart-field" name="pengirim" valueField="id_pengguna" textField="nama_pengguna" url="mod_pengguna/baca_pengguna" style="width:188px;"></input>
					</div>
				</div>
			</div>
			<div class="easyui-panel gray" noheader="true" border="false" style="width:272px;padding:0px 10px 0px 10px;margin-top:10px;">
				<div class="row">
					<div class="col12 cart-field-count">
						<input id="cal-screen" class="easyui-numberbox cart-field" name="jumlah_pembayaran" groupSeparator="." decimalSeparator="," style="width:252px;"></input>
					</div>
				</div>
			</div>
		</form>
		<div class="easyui-panel gray" noheader="true" border="false" style="width:272px;padding:0px 10px 10px 10px;">
			<div class="easyui-calculator">
				<div class="cal-row">
					<div class="cal-number">7</div>
					<div class="cal-number">8</div>
					<div class="cal-number">9</div>
					<div class="cal-button">Barang</div>
				</div>
				<div class="cal-row">
					<div class="cal-number">4</div>
					<div class="cal-number">5</div>
					<div class="cal-number">6</div>
					<div class="cal-button">Pengirim</div>
				</div>
				<div class="cal-row">
					<div class="cal-number">1</div>
					<div class="cal-number">2</div>
					<div class="cal-number">3</div>
					<div class="cal-button" onclick="hapus_keranjang();">Hapus</div>
				</div>
				<div class="cal-row">
					<div class="cal-number cal-number-span">0</div>
					<div class="cal-number cal-clear">C</div>
					<div class="cal-button cal-button-blue" onclick="simpan_transaksi();">Proses</div>
				</div>
			</div>
		</div>
	</div>
</div>