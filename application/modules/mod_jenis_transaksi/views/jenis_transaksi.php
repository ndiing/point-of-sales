<table id="datagrid_jenis_transaksi" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_jenis_transaksi" url="<?php echo base_url('mod_jenis_transaksi/baca_jenis_transaksi'); ?>">
	<thead>
		<tr>
			<th field="id_jenis_transaksi">id Jenis Transaksi</th>
			<th field="nama_jenis_transaksi">Jenis Transaksi</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_jenis_transaksi" class="easyui-dialog" buttons="#buttons_jenis_transaksi" closed="true" modal="true" style="height:150px;width:400px;">
	<form id="form_jenis_transaksi" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr>
					<th><label>id_jenis_transaksi</label></th>
					<th><input type="text" name="id_jenis_transaksi"></th>
				</tr>
				<tr>
					<th><label>nama_jenis_transaksi</label></th>
					<th><input type="text" name="nama_jenis_transaksi"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_jenis_transaksi">
	<a href="javascript:void(0);" plain="true" onclick="tambah_jenis_transaksi();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_jenis_transaksi();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_jenis_transaksi();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_jenis_transaksi">
	<a href="javascript:void(0);" onclick="simpan_jenis_transaksi();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_jenis_transaksi').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>