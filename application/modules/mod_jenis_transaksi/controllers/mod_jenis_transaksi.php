<?php

class Mod_jenis_transaksi extends MX_Controller
{
	public $table = 'jenis_transaksi';
	public $primary = 'id_jenis_transaksi';
	public function index() 
	{
		$this->load->view('jenis_transaksi');
	}
	public function validasi_jenis_transaksi()
	{
		$config = array(
			array(
				'field' => 'id_jenis_transaksi',
				'label' => 'id_jenis_transaksi',
				'rules' => ''
				),
			array(
				'field' => 'nama_jenis_transaksi',
				'label' => 'nama_jenis_transaksi',
				'rules' => ''
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_jenis_transaksi() 
	{
		if ($this->validasi_jenis_transaksi() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			'id_jenis_transaksi' => $_POST['id_jenis_transaksi'],
			'nama_jenis_transaksi' => $_POST['nama_jenis_transaksi'],					
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_jenis_transaksi() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_jenis_transaksi() 
	{
		if ($this->validasi_jenis_transaksi() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			'id_jenis_transaksi' => $_POST['id_jenis_transaksi'],
			'nama_jenis_transaksi' => $_POST['nama_jenis_transaksi'],					
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_jenis_transaksi() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>mysql_error()));
		}
	}
}