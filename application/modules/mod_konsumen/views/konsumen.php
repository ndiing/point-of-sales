<div class="easyui-tabs" fit="true" border="false">
	<div title="Konsumen">
		<table id="datagrid_konsumen" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_konsumen" url="<?php echo base_url('mod_konsumen/baca_konsumen'); ?>">
			<thead>
				<tr>
					<th field="id_konsumen">Kode Konsumen</th>
					<th field="nama_konsumen">Nama Konsumen</th>
					<th field="alamat">Alamat</th>
					<th field="telepon">Telepon</th>
					<th field="nama_instansi">Nama Instansi</th>
					<th field="nama_kategori_konsumen">Kategori</th>
					<th field="waktu_terdaftar">Waktu Terdaftar</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<div id="dialog_konsumen" class="easyui-dialog" buttons="#buttons_konsumen" closed="true" modal="true" style="height:250px;width:400px;">
			<form id="form_konsumen" method="post" class="easyui-form">
				<table>
					<tbody>
						<tr style="display:none;">
							<th><label>id_konsumen</label></th>
							<th><input type="hidden" name="id_konsumen"></th>
						</tr>
						<tr>
							<th><label>Nama Konsumen</label></th>
							<th><input type="text" name="nama_konsumen"></th>
						</tr>
						<tr>
							<th><label>Alamat</label></th>
							<th><input type="text" name="alamat"></th>
						</tr>
						<tr>
							<th><label>Telepon</label></th>
							<th><input type="text" name="telepon"></th>
						</tr>
						<tr>
							<th><label>Nama Instansi</label></th>
							<th><input type="text" name="nama_instansi"></th>
						</tr>
						<tr>
							<th><label>Kategori Konsumen</label></th>
							<th><input type="text" name="kategori_konsumen_id" class="easyui-combobox" valueField="id_kategori_konsumen" textField="nama_kategori_konsumen" url="mod_kategori_konsumen/baca_kategori_konsumen" style="width:267px;"></th>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div id="toolbar_konsumen">
			<a href="javascript:void(0);" plain="true" onclick="tambah_konsumen();" class="easyui-linkbutton">Tambah</a>
			<a href="javascript:void(0);" plain="true" onclick="edit_konsumen();" class="easyui-linkbutton">Edit</a>
			<a href="javascript:void(0);" plain="true" onclick="hapus_konsumen();" class="easyui-linkbutton">Hapus</a>
		</div>
		<div id="buttons_konsumen">
			<a href="javascript:void(0);" onclick="simpan_konsumen();" class="easyui-linkbutton">Simpan</a>
			<a href="javascript:void(0);" onclick="$('#dialog_konsumen').dialog('close');" class="easyui-linkbutton">Batal</a>
		</div>
	</div>
	<div title="Kategori" closable="true" href="mod_kategori_konsumen"></div>
</div>