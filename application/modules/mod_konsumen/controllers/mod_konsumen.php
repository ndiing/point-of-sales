<?php

class Mod_konsumen extends MX_Controller
{
	public $table = 'konsumen';
	public $primary = 'id_konsumen';
	public function index() 
	{
		$this->load->view('konsumen');
	}
	public function validasi_konsumen()
	{
		$config = array(
			array(
				'field' => 'id_konsumen',
				'label' => 'id_konsumen',
				'rules' => ''
				),
			array(
				'field' => 'nama_konsumen',
				'label' => 'nama_konsumen',
				'rules' => 'required'
				),
			array(
				'field' => 'alamat',
				'label' => 'alamat',
				'rules' => 'required'
				),
			array(
				'field' => 'telepon',
				'label' => 'telepon',
				'rules' => 'required'
				),
			array(
				'field' => 'nama_instansi',
				'label' => 'nama_instansi',
				'rules' => 'required'
				),
			array(
				'field' => 'kategori_konsumen_id',
				'label' => 'kategori_konsumen_id',
				'rules' => 'required'
				),
			array(
				'field' => 'waktu_terdaftar',
				'label' => 'waktu_terdaftar',
				'rules' => ''
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_konsumen() 
	{
		if ($this->validasi_konsumen() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_konsumen' => $_POST['id_konsumen'],
			'nama_konsumen' => $_POST['nama_konsumen'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nama_instansi' => $_POST['nama_instansi'],
			'kategori_konsumen_id' => $_POST['kategori_konsumen_id'],
			// 'waktu_terdaftar	' => $_POST['waktu_terdaftar	'],		
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_konsumen() 
	{
		$result = array();
		$query = $this->db
		->join('kategori_konsumen', 'id_kategori_konsumen = kategori_konsumen_id')
		->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_konsumen() 
	{
		if ($this->validasi_konsumen() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_konsumen' => $_POST['id_konsumen'],
			'nama_konsumen' => $_POST['nama_konsumen'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nama_instansi' => $_POST['nama_instansi'],
			'kategori_konsumen_id' => $_POST['kategori_konsumen_id'],
			// 'waktu_terdaftar' => $_POST['waktu_terdaftar'],					
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_konsumen() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}