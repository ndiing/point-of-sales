<table id="datagrid_keranjang" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_keranjang" url="<?php echo base_url('mod_keranjang/baca_keranjang'); ?>">
	<thead>
		<tr>
			<th field="id_keranjang">id_keranjang</th>
			<th field="kode_barang">kode_barang</th>
			<th field="jumlah">jumlah</th>
			<th field="ref_keranjang">ref_keranjang</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_keranjang" class="easyui-dialog" buttons="#buttons_keranjang" closed="true" modal="true" style="height:150px;width:400px;">
	<form id="form_keranjang" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr>
					<th><label>id_keranjang</label></th>
					<th><input type="text" name="id_keranjang"></th>
				</tr>
				<tr>
					<th><label>kode_barang</label></th>
					<th><input type="text" name="kode_barang"></th>
				</tr>
				<tr>
					<th><label>jumlah</label></th>
					<th><input type="text" name="jumlah"></th>
				</tr>
				<tr>
					<th><label>ref_keranjang</label></th>
					<th><input type="text" name="ref_keranjang"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_keranjang">
	<a href="javascript:void(0);" plain="true" onclick="tambah_keranjang();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_keranjang();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_keranjang();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_keranjang">
	<a href="javascript:void(0);" onclick="simpan_keranjang();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_keranjang').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>