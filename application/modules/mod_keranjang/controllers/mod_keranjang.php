<?php

class Mod_keranjang extends MX_Controller
{
	public $table = 'keranjang';
	public $primary = 'id_keranjang';
	public function index() 
	{
		// $this->load->view('keranjang');
	}
	public function buat_keranjang()
	{
		$data = array(
			'kode_barang' => 0,
			'jumlah' => 0,
			'ref_keranjang' => 0,
			);
		$query = $this->db->insert($this->table, $data);
		if ($query) {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function validasi_keranjang()
	{
		$config = array(
			array(
				'field' => 'id_keranjang',
				'label' => 'id_keranjang',
				'rules' => ''
				),
			array(
				'field' => 'kode_barang',
				'label' => 'kode_barang',
				'rules' => 'required'
				),
			array(
				'field' => 'jumlah',
				'label' => 'jumlah',
				'rules' => 'required'
				),
			array(
				'field' => 'ref_keranjang',
				'label' => 'ref_keranjang',
				'rules' => ''
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function ref_keranjang()
	{
		$query = $this->db
		->select_max('id_keranjang')
		->where('ref_keranjang', 0)
		->get($this->table)->row()->id_keranjang;
		return $query;
	}
	public function total_keranjang() 
	{
		$query = $this->db
		->where('ref_keranjang', $this->ref_keranjang())
		->join('gudang', 'kode = kode_barang')
		->select('sum(((modal + margin) * jumlah)) as total')
		->get($this->table)->row();
		echo json_encode($query->total);
	}
	public function tambah_keranjang() 
	{
		if ($this->validasi_keranjang() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_keranjang' => $_POST['id_keranjang'],
			'kode_barang' => $_POST['kode_barang'],
			'jumlah' => $_POST['jumlah'],
			'ref_keranjang' => $this->ref_keranjang()
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			$kurang_stok_gudang = modules::run('mod_gudang/kurang_stok_gudang', $data['kode_barang'], $data['jumlah']);
			if ($kurang_stok_gudang == TRUE) {
				echo json_encode(array('success'=>TRUE));
			}
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_keranjang() 
	{
		$result = array();
		$query = $this->db
		->where('ref_keranjang', $this->ref_keranjang())
		->join('gudang', 'kode = kode_barang')
		->join('jenis', 'id_jenis = jenis')
		->join('merk', 'id_merk = merk')
		->join('satuan', 'id_satuan = satuan')
		->get($this->table);
		foreach ($query->result() as $value) {
			$value->harga = $value->modal + $value->margin;
			$value->sub_total = $value->harga * $value->jumlah;
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_keranjang() 
	{
		if ($this->validasi_keranjang() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_keranjang' => $_POST['id_keranjang'],
			'kode_barang' => $_POST['kode_barang'],
			'jumlah' => $_POST['jumlah'],
			// 'ref_keranjang' => $_POST['ref_keranjang'],						
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_keranjang() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			$tambah_stok_gudang = modules::run('mod_gudang/tambah_stok_gudang', $_POST['kode'], $_POST['jumlah']);
			if ($tambah_stok_gudang==TRUE) {
				echo json_encode(array('success'=>TRUE));
			}
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}