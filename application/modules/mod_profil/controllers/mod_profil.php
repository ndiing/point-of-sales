<?php

class Mod_profil extends MX_Controller
{
	public $table = 'profil';
	public $primary = 'pengguna_id';
	public function index() 
	{
		$this->load->view('profil');
	}
	public function validasi_profil()
	{
		$config = array(
			array(
				'field' => 'pengguna_id',
				'label' => 'pengguna_id',
				'rules' => ''
				),
			array(
				'field' => 'nama_lengkap',
				'label' => 'nama_lengkap',
				'rules' => 'required'
				),
			array(
				'field' => 'alamat',
				'label' => 'alamat',
				'rules' => 'required'
				),
			array(
				'field' => 'telepon',
				'label' => 'telepon',
				'rules' => 'required'
				),
			array(
				'field' => 'nik',
				'label' => 'nik',
				'rules' => 'required'
				)
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_profil() 
	{
		if ($this->validasi_profil() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			'pengguna_id' => $_POST['pengguna_id'],
			'nama_lengkap' => $_POST['nama_lengkap'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nik' => $_POST['nik'],
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_profil() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_profil() 
	{
		if ($this->validasi_profil() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'pengguna_id' => $_POST['pengguna_id'],
			'nama_lengkap' => $_POST['nama_lengkap'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nik' => $_POST['nik'],				
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_profil() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}