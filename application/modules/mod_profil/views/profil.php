<table id="datagrid_profil" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_profil" url="<?php echo base_url('mod_profil/baca_profil'); ?>">
	<thead>
		<tr>
			<th field="pengguna_id">Kode Pengguna</th>
			<th field="nama_lengkap">Nama Lengkap</th>
			<th field="alamat">Alamat</th>
			<th field="telepon">Telepon</th>
			<th field="nik">Nik</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_profil" class="easyui-dialog" buttons="#buttons_profil" closed="true" modal="true" style="height:255px;width:400px;">
	<form id="form_profil" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr>
					<td><label>Kode Pengguna</label></td>
					<td><input type="text" name="pengguna_id" class="easyui-combobox" valueField="id_pengguna" textField="nama_pengguna" url="mod_pengguna/baca_pengguna" style="width:285px;"></td>
				</tr>
				<tr>
					<td><label>Nama Lengkap</label></td>
					<td><input type="text" name="nama_lengkap"></td>
				</tr>
				<tr>
					<td><label>Alamat</label></td>
					<td><input type="text" name="alamat"></td>
				</tr>
				<tr>
					<td><label>Telepon</label></td>
					<td><input type="text" name="telepon" class="easyui-numberbox"></td>
				</tr>
				<tr>
					<td><label>Nik</label></td>
					<td><input type="text" name="nik" class="easyui-numberbox"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_profil">
	<a href="javascript:void(0);" plain="true" onclick="tambah_profil();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_profil();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_profil();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_profil">
	<a href="javascript:void(0);" onclick="simpan_profil();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_profil').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>