<div class="easyui-tabs" fit="true" border="false">
	<div title="Pengguna">
		<table id="datagrid_pengguna" class="easyui-datagrid" striped="true" border="false" fit="true" toolbar="#toolbar_pengguna" url="<?php echo base_url('mod_pengguna/baca_pengguna'); ?>">
			<thead>
				<tr>
					<th field="id_pengguna" width="100">Kode Pengguna</th>
					<th field="nama_pengguna" width="100">Nama Pengguna</th>
					<th field="nama_kelompok" width="100">Kelompok</th>
					<th field="waktu_terdaftar" width="100">Waktu Terdaftar</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<div id="dialog_pengguna" class="easyui-dialog" buttons="#buttons_pengguna" closed="true" modal="true" style="height:200px;width:400px;">
			<form id="form_pengguna" method="post" class="easyui-form">
				<table>
					<tbody>
						<tr style="display:none;">
							<td><label>Kode Pengguna</label></td>
							<td><input type="hidden" name="id_pengguna"></td>
						</tr>
						<tr>
							<td><label>Nama Pengguna</label></td>
							<td><input type="text" name="nama_pengguna"></td>
						</tr>
						<tr>
							<td><label>Sandi</label></td>
							<td><input type="text" name="sandi"></td>
						</tr>
						<tr>
							<td><label>Kelompok</label></td>
							<td><input type="text" name="kelompok" class="easyui-combobox" valueField="id_kelompok" textField="nama_kelompok" url="mod_kelompok/baca_kelompok" style="width:282px;"></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div id="toolbar_pengguna">
			<a href="javascript:void(0);" plain="true" onclick="tambah_pengguna();" class="easyui-linkbutton">Tambah</a>
			<a href="javascript:void(0);" plain="true" onclick="edit_pengguna();" class="easyui-linkbutton">Edit</a>
			<a href="javascript:void(0);" plain="true" onclick="hapus_pengguna();" class="easyui-linkbutton">Hapus</a>
		</div>
		<div id="buttons_pengguna">
			<a href="javascript:void(0);" onclick="simpan_pengguna();" class="easyui-linkbutton">Simpan</a>
			<a href="javascript:void(0);" onclick="$('#dialog_pengguna').dialog('close');" class="easyui-linkbutton">Batal</a>
		</div>
	</div>
	<div title="Kelompok" href="mod_kelompok" closable="true"></div>
	<div title="Profil" href="mod_profil" closable="true"></div>
</div>