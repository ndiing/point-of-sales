<?php

class Mod_pengguna extends MX_Controller
{
	public $table = 'pengguna';
	public $primary = 'id_pengguna';
	public function index() 
	{
		$this->load->view('pengguna');
	}
	public function validasi_pengguna()
	{
		$config = array(
			array(
				'field' => 'id_pengguna',
				'label' => 'id_pengguna',
				'rules' => '',
				),
			array(
				'field' => 'kelompok',
				'label' => 'kelompok',
				'rules' => 'required',
				),
			array(
				'field' => 'nama_pengguna',
				'label' => 'nama_pengguna',
				'rules' => 'required',
				),
			array(
				'field' => 'sandi',
				'label' => 'sandi',
				'rules' => 'required',
				),
			array(
				'field' => 'token',
				'label' => 'token',
				'rules' => '',
				),
			array(
				'field' => 'waktu_terdaftar',
				'label' => 'waktu_terdaftar',
				'rules' => '',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_pengguna() 
	{
		if ($this->validasi_pengguna() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_pengguna' => $_POST['id_pengguna'],							
			'kelompok' => $_POST['kelompok'],							
			'nama_pengguna' => $_POST['nama_pengguna'],							
			'sandi' => sha1($_POST['sandi']+md5($_POST['sandi'])+$_POST['sandi']),							
			'token' => md5($_POST['sandi']),		
			// 'waktu_terdaftar' => $_POST['waktu_terdaftar'],							
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_pengguna() 
	{
		$result = array();
		$query = $this->db
		->join('kelompok', 'id_kelompok = kelompok')
		->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_pengguna() 
	{
		if ($this->validasi_pengguna() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_pengguna' => $_POST['id_pengguna'],							
			'kelompok' => $_POST['kelompok'],							
			'nama_pengguna' => $_POST['nama_pengguna'],							
			'sandi' => sha1($_POST['sandi']+md5($_POST['sandi'])+$_POST['sandi']),							
			'token' => md5($_POST['sandi']),						
			// 'waktu_terdaftar' => $_POST['waktu_terdaftar'],							
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_pengguna() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>mysql_error()));
		}
	}
}