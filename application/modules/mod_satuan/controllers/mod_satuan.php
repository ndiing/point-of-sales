<?php

class Mod_satuan extends MX_Controller
{
	public $table = 'satuan';
	public $primary = 'id_satuan';
	public function index() 
	{
		$this->load->view('satuan');
	}
	public function validasi_satuan()
	{
		$config = array(
			array(
				'field' => 'id_satuan',
				'label' => 'id_satuan',
				'rules' => '',
				),
			array(
				'field' => 'nama_satuan',
				'label' => 'nama_satuan',
				'rules' => 'required',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_satuan() 
	{
		if ($this->validasi_satuan() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_satuan' => $_POST['id_satuan'],
			'nama_satuan' => $_POST['nama_satuan'],				
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_satuan() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_satuan() 
	{
		if ($this->validasi_satuan() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_satuan' => $_POST['id_satuan'],
			'nama_satuan' => $_POST['nama_satuan'],						
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_satuan() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}