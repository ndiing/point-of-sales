<table id="datagrid_satuan" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_satuan" url="<?php echo base_url('mod_satuan/baca_satuan'); ?>">
	<thead>
		<tr>
			<th field="id_satuan">Kode Satuan</th>
			<th field="nama_satuan">Nama Satuan</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_satuan" class="easyui-dialog" buttons="#buttons_satuan" closed="true" modal="true" style="height:150px;width:400px;">
	<form id="form_satuan" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr style="display:none;">
					<th><label>Kode Satuan</label></th>
					<th><input type="hidden" name="id_satuan"></th>
				</tr>
				<tr>
					<th><label>Nama Satuan</label></th>
					<th><input type="text" name="nama_satuan"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_satuan">
	<a href="javascript:void(0);" plain="true" onclick="tambah_satuan();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_satuan();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_satuan();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_satuan">
	<a href="javascript:void(0);" onclick="simpan_satuan();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_satuan').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>