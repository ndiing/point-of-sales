<div class="easyui-tabs" fit="true" border="false">
	<div title="Penjualan">
		<table id="datagrid_transaksi" class="easyui-datagrid" border="false" fit="true" url="<?php echo base_url('mod_transaksi/baca_transaksi'); ?>">
			<thead>
				<tr>
					<th field="keranjang_id">Kode Keranjang</th>
					<th field="jumlah_pembayaran">Jumlah Pembayaran</th>
					<th field="jumlah_kembalian">Jumlah Kembalian</th>
					<th field="nama_konsumen">Konsumen</th>
					<th field="nama_jenis_transaksi">Jenis Transaksi</th>
					<th field="pengirim">Pengirim</th>
					<th field="pengguna_id">Kasir</th>
					<th field="waktu_transaksi">Waktu Transaksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	<div title="Pengiriman"></div>
</div>