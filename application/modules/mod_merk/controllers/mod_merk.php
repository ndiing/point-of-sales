<?php

class Mod_merk extends MX_Controller
{
	public $table = 'merk';
	public $primary = 'id_merk';
	public function index() 
	{
		$this->load->view('merk');
	}
	public function validasi_merk()
	{
		$config = array(
			array(
				'field' => 'id_merk',
				'label' => 'id_merk',
				'rules' => '',
				),
			array(
				'field' => 'nama_merk',
				'label' => 'nama_merk',
				'rules' => 'required',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_merk() 
	{
		if ($this->validasi_merk() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_merk' => $_POST['id_merk'],
			'nama_merk' => $_POST['nama_merk'],				
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_merk() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_merk() 
	{
		if ($this->validasi_merk() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_merk' => $_POST['id_merk'],
			'nama_merk' => $_POST['nama_merk'],						
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_merk() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
}