<table id="datagrid_merk" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_merk" url="<?php echo base_url('mod_merk/baca_merk'); ?>">
	<thead>
		<tr>
			<th field="id_merk">Kode Merk</th>
			<th field="nama_merk">Nama Merk</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_merk" class="easyui-dialog" buttons="#buttons_merk" closed="true" modal="true" style="height:150px;width:400px;">
	<form id="form_merk" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr style="display:none;">
					<th><label>Kode Merk</label></th>
					<th><input type="hidden" name="id_merk"></th>
				</tr>
				<tr>
					<th><label>Nama Merk</label></th>
					<th><input type="text" name="nama_merk"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_merk">
	<a href="javascript:void(0);" plain="true" onclick="tambah_merk();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_merk();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_merk();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_merk">
	<a href="javascript:void(0);" onclick="simpan_merk();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_merk').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>