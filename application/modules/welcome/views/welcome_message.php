<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Point Of Sales</title>
	<link rel="stylesheet" href="assets/themes/metro/easyui.css">
	<script src="assets/jquery.min.js"></script>
	<script src="assets/jquery.easyui.min.js"></script>
	<script src="assets/jquery.application.js"></script>
	<script src="assets/locale/easyui-lang-id.js"></script>
</head>
<body class="easyui-layout">
	<div region="west" border="false" split="true" href="mod_menu" class="dark" style="width:225px"></div>
	<div region="center" border="false" style="height:100px">
		<div id="panel_welcome" class="easyui-panel" border="false" fit="true" href="mod_beranda"></div>
	</div>
</body>
</html>