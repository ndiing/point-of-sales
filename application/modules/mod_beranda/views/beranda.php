<div class="easyui-panel" border="false" fit="true" title="Beranda" iconCls="icon-demo" style="padding:10px;">
	<div class="row">

		<div class="col3">
			<div class="info info-green">
				<div class="info-body">
					<div class="row">
						<div class="col4"><div class="icon-demo-large"></div></div>
						<div class="col8">
							<h1>198</h1>
							<p>Total Produk</p>
						</div>
					</div>
				</div>
				<div class="info-button">Lihat lebih lanjut</div>
			</div>
		</div>

		<div class="col3">
			<div class="info info-greenlight">
				<div class="info-body">
					<div class="row">
						<div class="col4"><div class="icon-demo-large"></div></div>
						<div class="col8">
							<h1>15</h1>
							<p>Penjualan Hari Ini</p>
						</div>
					</div>
				</div>
				<div class="info-button">Lihat lebih lanjut</div>
			</div>
		</div>

		<div class="col3">
			<div class="info info-purple">
				<div class="info-body">
					<div class="row">
						<div class="col4"><div class="icon-demo-large"></div></div>
						<div class="col8">
							<h1>126</h1>
							<p>Pelanggan Terdaftar</p>
						</div>
					</div>
				</div>
				<div class="info-button">Lihat lebih lanjut</div>
			</div>
		</div>

		<div class="col3">
			<div class="info info-blue">
				<div class="info-body">
					<div class="row">
						<div class="col4"><div class="icon-demo-large"></div></div>
						<div class="col8">
							<h1>1.958.500</h1>
							<p>Total Pemasukan</p>
						</div>
					</div>
				</div>
				<div class="info-button">Lihat lebih lanjut</div>
			</div>
		</div>

	</div>
	<script src="assets/highcharts/highcharts.js"></script>
	<script>
	$(function () {
	  $('#container').highcharts({
	      chart: {
	        type: 'line'
	      },
	      title: {
	        text: ''
	      },
	      xAxis: {
	        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des']
	      },
	      yAxis: {
	        title: {
	          text: ''
	        }
	      },
	      series: [{
	          name: 'Keluar',
	          data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
	        }, {
	          name: 'Masuk',
	          data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
	        }
	      ]
	    });
	});
	</script>
	<br>
	<div class="easyui-panel" border="false" iconCls="icon-demo" title="Grafik Penjualan" style="height:430px;padding-top:10px;">
		<div id="container" style="height:380px;"></div>
	</div>
</div>
