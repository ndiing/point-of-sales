<table id="datagrid_jenis" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_jenis" url="<?php echo base_url('mod_jenis/baca_jenis'); ?>">
	<thead>
		<tr>
			<th field="id_jenis">Kode Jenis</th>
			<th field="nama_jenis">Nama Jenis</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_jenis" class="easyui-dialog" buttons="#buttons_jenis" closed="true" modal="true" style="height:150px;width:400px;">
	<form id="form_jenis" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr style="display:none;">
					<th><label>Id Jenis</label></th>
					<th><input type="hidden" name="id_jenis"></th>
				</tr>
				<tr>
					<th><label>Nama Jenis</label></th>
					<th><input type="text" name="nama_jenis"></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_jenis">
	<a href="javascript:void(0);" plain="true" onclick="tambah_jenis();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_jenis();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_jenis();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_jenis">
	<a href="javascript:void(0);" onclick="simpan_jenis();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_jenis').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>