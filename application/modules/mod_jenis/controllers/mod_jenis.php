<?php

class Mod_jenis extends MX_Controller
{
	public $table = 'jenis';
	public $primary = 'id_jenis';
	public function index() 
	{
		$this->load->view('jenis');
	}
	public function validasi_jenis()
	{
		$config = array(
			array(
				'field' => 'id_jenis',
				'label' => 'id_jenis',
				'rules' => '',
				),
			array(
				'field' => 'nama_jenis',
				'label' => 'nama_jenis',
				'rules' => 'required',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_jenis() 
	{
		if ($this->validasi_jenis() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_jenis' => $_POST['id_jenis'],
			'nama_jenis' => $_POST['nama_jenis'],				
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_jenis() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_jenis() 
	{
		if ($this->validasi_jenis() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			// 'id_jenis' => $_POST['id_jenis'],
			'nama_jenis' => $_POST['nama_jenis'],						
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_jenis() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>mysql_error()));
		}
	}
}