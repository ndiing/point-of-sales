<table id="datagrid_kelompok" class="easyui-datagrid" border="false" fit="true" toolbar="#toolbar_kelompok" url="<?php echo base_url('mod_kelompok/baca_kelompok'); ?>">
	<thead>
		<tr>
			<th field="id_kelompok">Kode Kelompok</th>
			<th field="nama_kelompok">Nama Kelompok</th>
			<th field="v_menambahkan">Menambahkan</th>
			<th field="v_melihat">Melihat</th>
			<th field="v_merubah">Merubah</th>
			<th field="v_menghapus">Menghapus</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<div id="dialog_kelompok" class="easyui-dialog" buttons="#buttons_kelompok" closed="true" modal="true" style="height:250px;width:400px;">
	<form id="form_kelompok" method="post" class="easyui-form">
		<table>
			<tbody>
				<tr style="display:none;">
					<th><label>Kode Kelompok</label></th>
					<th><input type="hidden" name="id_kelompok"></th>
				</tr>
				<tr>
					<th><label>Nama Kelompok</label></th>
					<th><input type="text" name="nama_kelompok"></th>
				</tr>
				<tr>
					<th><label>Menambahkan</label></th>
					<th><select type="text" name="menambahkan" class="easyui-combobox" mode="local" editable="false" style="width:285px;">
						<option value="1">Ya</option>
						<option value="0">Tidak</option>
					</select></th>
				</tr>
				<tr>
					<th><label>Melihat</label></th>
					<th><select type="text" name="melihat" class="easyui-combobox" mode="local" editable="false" style="width:285px;">
						<option value="1">Ya</option>
						<option value="0">Tidak</option>
					</select></th>
				</tr>
				<tr>
					<th><label>Merubah</label></th>
					<th><select type="text" name="merubah" class="easyui-combobox" mode="local" editable="false" style="width:285px;">
						<option value="1">Ya</option>
						<option value="0">Tidak</option>
					</select></th>
				</tr>
				<tr>
					<th><label>Menghapus</label></th>
					<th><select type="text" name="menghapus" class="easyui-combobox" mode="local" editable="false" style="width:285px;">
						<option value="1">Ya</option>
						<option value="0">Tidak</option>
					</select></th>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div id="toolbar_kelompok">
	<a href="javascript:void(0);" plain="true" onclick="tambah_kelompok();" class="easyui-linkbutton">Tambah</a>
	<a href="javascript:void(0);" plain="true" onclick="edit_kelompok();" class="easyui-linkbutton">Edit</a>
	<a href="javascript:void(0);" plain="true" onclick="hapus_kelompok();" class="easyui-linkbutton">Hapus</a>
</div>
<div id="buttons_kelompok">
	<a href="javascript:void(0);" onclick="simpan_kelompok();" class="easyui-linkbutton">Simpan</a>
	<a href="javascript:void(0);" onclick="$('#dialog_kelompok').dialog('close');" class="easyui-linkbutton">Batal</a>
</div>