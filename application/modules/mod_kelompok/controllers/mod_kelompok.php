<?php

class Mod_kelompok extends MX_Controller
{
	public $table = 'kelompok';
	public $primary = 'id_kelompok';
	public function index() 
	{
		$this->load->view('kelompok');
	}
	public function validasi_kelompok()
	{
		$config = array(
			array(
				'field' => 'id_kelompok',
				'label' => 'id_kelompok',
				'rules' => '',
				),
			array(
				'field' => 'nama_kelompok',
				'label' => 'nama_kelompok',
				'rules' => '',
				),
			array(
				'field' => 'menambahkan',
				'label' => 'menambahkan',
				'rules' => '',
				),
			array(
				'field' => 'melihat',
				'label' => 'melihat',
				'rules' => '',
				),
			array(
				'field' => 'merubah',
				'label' => 'merubah',
				'rules' => '',
				),
			array(
				'field' => 'menghapus',
				'label' => 'menghapus',
				'rules' => '',
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) 
			{
				return FALSE;
			} 
			else 
			{
				return TRUE;
			}
	}
	public function tambah_kelompok() 
	{
		if ($this->validasi_kelompok() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			'id_kelompok' => $_POST['id_kelompok'],
			'nama_kelompok' => $_POST['nama_kelompok'],				
			'menambahkan' => $_POST['menambahkan'],
			'melihat' => $_POST['melihat'],
			'merubah' => $_POST['merubah'],
			'menghapus' => $_POST['menghapus'],
			);

		$query = $this->db->insert($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function baca_kelompok() 
	{
		$result = array();
		$query = $this->db->get($this->table);
		foreach ($query->result() as $value) {
			if ($value->menambahkan==0) {
				$value->v_menambahkan = 'Tidak';
			} else {
				$value->v_menambahkan = 'Ya';
			}
			if ($value->melihat==0) {
				$value->v_melihat = 'Tidak';
			} else {
				$value->v_melihat = 'Ya';
			}
			if ($value->merubah==0) {
				$value->v_merubah = 'Tidak';
			} else {
				$value->v_merubah = 'Ya';
			}
			if ($value->menghapus==0) {
				$value->v_menghapus = 'Tidak';
			} else {
				$value->v_menghapus = 'Ya';
			}
			array_push($result, $value);
		}
		echo json_encode($result);
	}
	public function rubah_kelompok() 
	{
		if ($this->validasi_kelompok() == FALSE) {
			echo json_encode(array('msg'=>'ERROR!'));
			return FALSE;
		}

		$data = array(
			'id_kelompok' => $_POST['id_kelompok'],
			'nama_kelompok' => $_POST['nama_kelompok'],					
			'menambahkan' => $_POST['menambahkan'],
			'melihat' => $_POST['melihat'],
			'merubah' => $_POST['merubah'],
			'menghapus' => $_POST['menghapus'],	
			);

		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->update($this->table, $data);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>'ERROR saving'));
		}
	}
	public function hapus_kelompok() 
	{
		$query = $this->db
		->where($this->primary, $_POST[$this->primary])
		->delete($this->table);

		if ($query) {
			echo json_encode(array('success'=>TRUE));
		}
		else
		{
			echo json_encode(array('msg'=>mysql_error()));
		}
	}
}