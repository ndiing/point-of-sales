var url;
function tambah_jenis(){
	$('#dialog_jenis').dialog('open').dialog('setTitle','New User');
	$('#form_jenis').form('clear');
	url = wlhref+'mod_jenis/tambah_jenis';
}
function edit_jenis(){
	var row = $('#datagrid_jenis').datagrid('getSelected');
	if (row){
		$('#dialog_jenis').dialog('open').dialog('setTitle','Edit User');
		$('#form_jenis').form('load',row);
		url = wlhref+'mod_jenis/rubah_jenis';
	}
}
function simpan_jenis(){
	$('#form_jenis').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_jenis').dialog('close');
				$('#datagrid_jenis').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_jenis(){
	var row = $('#datagrid_jenis').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_jenis/hapus_jenis',{id_jenis:row.id_jenis},function(result){
					if (result.success){
						$('#datagrid_jenis').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}