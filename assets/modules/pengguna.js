var url;
function tambah_pengguna(){
	$('#dialog_pengguna').dialog('open').dialog('setTitle','New User');
	$('#form_pengguna').form('clear');
	url = wlhref+'mod_pengguna/tambah_pengguna';
}
function edit_pengguna(){
	var row = $('#datagrid_pengguna').datagrid('getSelected');
	if (row){
		$('#dialog_pengguna').dialog('open').dialog('setTitle','Edit User');
		$('#form_pengguna').form('load',row);
		url = wlhref+'mod_pengguna/rubah_pengguna';
	}
}
function simpan_pengguna(){
	$('#form_pengguna').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_pengguna').dialog('close');
				$('#datagrid_pengguna').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_pengguna(){
	var row = $('#datagrid_pengguna').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_pengguna/hapus_pengguna',{id_pengguna:row.id_pengguna},function(result){
					if (result.success){
						$('#datagrid_pengguna').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}