var url;
function tambah_satuan(){
	$('#dialog_satuan').dialog('open').dialog('setTitle','New User');
	$('#form_satuan').form('clear');
	url = wlhref+'mod_satuan/tambah_satuan';
}
function edit_satuan(){
	var row = $('#datagrid_satuan').datagrid('getSelected');
	if (row){
		$('#dialog_satuan').dialog('open').dialog('setTitle','Edit User');
		$('#form_satuan').form('load',row);
		url = wlhref+'mod_satuan/rubah_satuan';
	}
}
function simpan_satuan(){
	$('#form_satuan').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_satuan').dialog('close');
				$('#datagrid_satuan').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_satuan(){
	var row = $('#datagrid_satuan').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_satuan/hapus_satuan',{id_satuan:row.id_satuan},function(result){
					if (result.success){
						$('#datagrid_satuan').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}