var url;
function tambah_keranjang(){
	// $('#dialog_keranjang').dialog('open').dialog('setTitle','New User');
	// $('#form_keranjang').form('clear');
	// url = wlhref+'mod_keranjang/tambah_keranjang';
}
function edit_keranjang(){
	// var row = $('#datagrid_keranjang').datagrid('getSelected');
	// if (row){
	// 	$('#dialog_keranjang').dialog('open').dialog('setTitle','Edit User');
	// 	$('#form_keranjang').form('load',row);
	// 	url = wlhref+'mod_keranjang/rubah_keranjang';
	// }
}
function simpan_keranjang(){
	$('#form_keranjang').form('submit',{
		url: wlhref+'mod_keranjang/tambah_keranjang',
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				// $('#dialog_keranjang').dialog('close');
				$('#form_keranjang').form('clear');
				$('#datagrid_keranjang').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_keranjang(){
	var row = $('#datagrid_keranjang').datagrid('getSelected');
	if (row){
		$.messager.confirm('Pembatalan',' Apakah anda yakin akan membatalkan penjualan untuk barang yang di pilih?',function(r){
			if (r){
				$.post(wlhref+'mod_keranjang/hapus_keranjang',{
					'id_keranjang': row.id_keranjang, 
					'kode': row.kode, 
					'jumlah': row.jumlah
				},function(result){
					if (result.success){
						$('#datagrid_keranjang').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}