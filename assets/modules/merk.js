var url;
function tambah_merk(){
	$('#dialog_merk').dialog('open').dialog('setTitle','New User');
	$('#form_merk').form('clear');
	url = wlhref+'mod_merk/tambah_merk';
}
function edit_merk(){
	var row = $('#datagrid_merk').datagrid('getSelected');
	if (row){
		$('#dialog_merk').dialog('open').dialog('setTitle','Edit User');
		$('#form_merk').form('load',row);
		url = wlhref+'mod_merk/rubah_merk';
	}
}
function simpan_merk(){
	$('#form_merk').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_merk').dialog('close');
				$('#datagrid_merk').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_merk(){
	var row = $('#datagrid_merk').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_merk/hapus_merk',{id_merk:row.id_merk},function(result){
					if (result.success){
						$('#datagrid_merk').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}