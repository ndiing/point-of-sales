var url;
function tambah_gudang(){
	$('#dialog_gudang').dialog('open').dialog('setTitle','New User');
	$('#form_gudang').form('clear');
	url = wlhref+'mod_gudang/tambah_gudang';
}
function edit_gudang(){
	var row = $('#datagrid_gudang').datagrid('getSelected');
	if (row){
		$('#dialog_gudang').dialog('open').dialog('setTitle','Edit User');
		$('#form_gudang').form('load',row);
		url = wlhref+'mod_gudang/rubah_gudang';
	}
}
function simpan_gudang(){
	$('#form_gudang').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_gudang').dialog('close');
				$('#datagrid_gudang').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_gudang(){
	var row = $('#datagrid_gudang').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_gudang/hapus_gudang',{kode:row.kode},function(result){
					if (result.success){
						$('#datagrid_gudang').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}