var url;
function tambah_jenis_transaksi(){
	$('#dialog_jenis_transaksi').dialog('open').dialog('setTitle','New User');
	$('#form_jenis_transaksi').form('clear');
	url = wlhref+'mod_jenis_transaksi/tambah_jenis_transaksi';
}
function edit_jenis_transaksi(){
	var row = $('#datagrid_jenis_transaksi').datagrid('getSelected');
	if (row){
		$('#dialog_jenis_transaksi').dialog('open').dialog('setTitle','Edit User');
		$('#form_jenis_transaksi').form('load',row);
		url = wlhref+'mod_jenis_transaksi/rubah_jenis_transaksi';
	}
}
function simpan_jenis_transaksi(){
	$('#form_jenis_transaksi').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_jenis_transaksi').dialog('close');
				$('#datagrid_jenis_transaksi').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_jenis_transaksi(){
	var row = $('#datagrid_jenis_transaksi').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_jenis_transaksi/hapus_jenis_transaksi',{kode:row.kode},function(result){
					if (result.success){
						$('#datagrid_jenis_transaksi').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}