var url;
function tambah_transaksi(){
	// $('#dialog_transaksi').dialog('open').dialog('setTitle','New User');
	// $('#form_transaksi').form('clear');
	// url = wlhref+'mod_transaksi/tambah_transaksi';
}
function edit_transaksi(){
	var row = $('#datagrid_transaksi').datagrid('getSelected');
	if (row){
		$('#dialog_transaksi').dialog('open').dialog('setTitle','Edit User');
		$('#form_transaksi').form('load',row);
		url = wlhref+'mod_transaksi/rubah_transaksi';
	}
}
function simpan_transaksi(){
	$('#form_transaksi').form('submit',{
		url : wlhref+'mod_transaksi/tambah_transaksi',
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				// $('#dialog_transaksi').dialog('close');
				$('#datagrid_keranjang').datagrid('reload');
				$('#cal-total').numberbox('setValue', 0);
				$('#cal-screen').numberbox('setValue', 0);
				$('#datagrid_transaksi').datagrid('reload');
				$.messager.alert('Notifikasi', result.msg);
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_transaksi(){
	var row = $('#datagrid_transaksi').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_transaksi/hapus_transaksi',{kode:row.kode},function(result){
					if (result.success){
						$('#datagrid_transaksi').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}