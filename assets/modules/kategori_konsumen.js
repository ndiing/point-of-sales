var url;
function tambah_kategori_konsumen(){
	$('#dialog_kategori_konsumen').dialog('open').dialog('setTitle','New User');
	$('#form_kategori_konsumen').form('clear');
	url = wlhref+'mod_kategori_konsumen/tambah_kategori_konsumen';
}
function edit_kategori_konsumen(){
	var row = $('#datagrid_kategori_konsumen').datagrid('getSelected');
	if (row){
		$('#dialog_kategori_konsumen').dialog('open').dialog('setTitle','Edit User');
		$('#form_kategori_konsumen').form('load',row);
		url = wlhref+'mod_kategori_konsumen/rubah_kategori_konsumen';
	}
}
function simpan_kategori_konsumen(){
	$('#form_kategori_konsumen').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_kategori_konsumen').dialog('close');
				$('#datagrid_kategori_konsumen').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_kategori_konsumen(){
	var row = $('#datagrid_kategori_konsumen').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_kategori_konsumen/hapus_kategori_konsumen',{id_kategori_konsumen:row.id_kategori_konsumen},function(result){
					if (result.success){
						$('#datagrid_kategori_konsumen').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}