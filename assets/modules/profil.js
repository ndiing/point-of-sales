var url;
function tambah_profil(){
	$('#dialog_profil').dialog('open').dialog('setTitle','New User');
	$('#form_profil').form('clear');
	url = wlhref+'mod_profil/tambah_profil';
}
function edit_profil(){
	var row = $('#datagrid_profil').datagrid('getSelected');
	if (row){
		$('#dialog_profil').dialog('open').dialog('setTitle','Edit User');
		$('#form_profil').form('load',row);
		url = wlhref+'mod_profil/rubah_profil';
	}
}
function simpan_profil(){
	$('#form_profil').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_profil').dialog('close');
				$('#datagrid_profil').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_profil(){
	var row = $('#datagrid_profil').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_profil/hapus_profil',{pengguna_id:row.pengguna_id},function(result){
					if (result.success){
						$('#datagrid_profil').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}