var url;
function tambah_konsumen(){
	$('#dialog_konsumen').dialog('open').dialog('setTitle','New User');
	$('#form_konsumen').form('clear');
	url = wlhref+'mod_konsumen/tambah_konsumen';
}
function edit_konsumen(){
	var row = $('#datagrid_konsumen').datagrid('getSelected');
	if (row){
		$('#dialog_konsumen').dialog('open').dialog('setTitle','Edit User');
		$('#form_konsumen').form('load',row);
		url = wlhref+'mod_konsumen/rubah_konsumen';
	}
}
function simpan_konsumen(){
	$('#form_konsumen').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_konsumen').dialog('close');
				$('#datagrid_konsumen').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_konsumen(){
	var row = $('#datagrid_konsumen').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_konsumen/hapus_konsumen',{id_konsumen:row.id_konsumen},function(result){
					if (result.success){
						$('#datagrid_konsumen').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}