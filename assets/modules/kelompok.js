var url;
function tambah_kelompok(){
	$('#dialog_kelompok').dialog('open').dialog('setTitle','New User');
	$('#form_kelompok').form('clear');
	url = wlhref+'mod_kelompok/tambah_kelompok';
}
function edit_kelompok(){
	var row = $('#datagrid_kelompok').datagrid('getSelected');
	if (row){
		$('#dialog_kelompok').dialog('open').dialog('setTitle','Edit User');
		$('#form_kelompok').form('load',row);
		url = wlhref+'mod_kelompok/rubah_kelompok';
	}
}
function simpan_kelompok(){
	$('#form_kelompok').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.success){
				$('#dialog_kelompok').dialog('close');
				$('#datagrid_kelompok').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}
function hapus_kelompok(){
	var row = $('#datagrid_kelompok').datagrid('getSelected');
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				$.post(wlhref+'mod_kelompok/hapus_kelompok',{id_kelompok:row.id_kelompok},function(result){
					if (result.success){
						$('#datagrid_kelompok').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}