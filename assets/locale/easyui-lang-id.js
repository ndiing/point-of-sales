if ($.fn.pagination){
	$.fn.pagination.defaults.beforePageText = 'Halaman';
	$.fn.pagination.defaults.afterPageText = 'dari {pages}';
	$.fn.pagination.defaults.displayMsg = 'Menampilkan {from} hingga {to} dari {total} item';
}
if ($.fn.datagrid){
	$.fn.datagrid.defaults.loadMsg = 'Permintaan sedang dalam proses ...';
}
if ($.fn.treegrid && $.fn.datagrid){
	$.fn.treegrid.defaults.loadMsg = $.fn.datagrid.defaults.loadMsg;
}
if ($.messager){
	$.messager.defaults.ok = 'Ya';
	$.messager.defaults.cancel = 'Batal';
}
if ($.fn.validatebox){
	$.fn.validatebox.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
	$.fn.validatebox.defaults.rules.email.message = 'Inputkan alamat email yang valid.';
	$.fn.validatebox.defaults.rules.url.message = 'Inputkan URL yang valid.';
	$.fn.validatebox.defaults.rules.length.message = 'Inputkan nilai antara {0} dan {1}.';
	$.fn.validatebox.defaults.rules.remote.message = 'Perbaiki inputan ini.';
}
if ($.fn.numberbox){
	$.fn.numberbox.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
}
if ($.fn.combobox){
	$.fn.combobox.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
}
if ($.fn.combotree){
	$.fn.combotree.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
}
if ($.fn.combogrid){
	$.fn.combogrid.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
}
if ($.fn.calendar){
	$.fn.calendar.defaults.weeks = ['M','S','S','R','K','J','S'];
	$.fn.calendar.defaults.months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
}
if ($.fn.datebox){
	$.fn.datebox.defaults.currentText = 'Hari Ini';
	$.fn.datebox.defaults.closeText = 'Tutup';
	$.fn.datebox.defaults.okText = 'Ya';
	$.fn.datebox.defaults.missingMessage = 'Tidak boleh dibiarkan kosong.';
}
if ($.fn.datetimebox && $.fn.datebox){
	$.extend($.fn.datetimebox.defaults,{
		currentText: $.fn.datebox.defaults.currentText,
		closeText: $.fn.datebox.defaults.closeText,
		okText: $.fn.datebox.defaults.okText,
		missingMessage: $.fn.datebox.defaults.missingMessage
	});
}
