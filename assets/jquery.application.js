var wlhref = window.location;
$(function () {
	// Start Loader
	var load = {
		folder: 'assets/modules/',
		placed: $('head'),
		jsfile: [
		'gudang', 
		'jenis', 
		'jenis_transaksi', 
		'kategori_konsumen', 
		'kelompok', 
		'keranjang', 
		'konsumen', 
		'merk', 
		'pengguna',  
		'profil',
		'satuan',
		'transaksi',
		]
	}
	$.each(load.jsfile, function () {
		$('<script>', {'src': wlhref+load.folder+this+'.js'}).appendTo(load.placed);
	});
	// End Loader

	// Buka Transaksi dengan F2
	$(window).keydown(function (e) {
		if (e.keyCode==113) {
			$('#transaksi').click();
		}
	});

});

function openPanel (obj) {
	return $('#panel_welcome').panel('refresh', obj);
}