-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2013 at 07:01 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE IF NOT EXISTS `gudang` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(45) DEFAULT NULL,
  `jenis` int(11) DEFAULT NULL,
  `merk` int(11) DEFAULT NULL,
  `satuan` int(11) DEFAULT NULL,
  `modal` int(11) DEFAULT '0',
  `margin` int(11) DEFAULT '0',
  `stok` int(11) DEFAULT '0',
  `waktu_catat` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`kode`),
  KEY `jenis` (`jenis`),
  KEY `merk` (`merk`),
  KEY `satuan` (`satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gudang`
--


-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jenis`
--


-- --------------------------------------------------------

--
-- Table structure for table `jenis_transaksi`
--

CREATE TABLE IF NOT EXISTS `jenis_transaksi` (
  `id_jenis_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_transaksi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jenis_transaksi`
--


-- --------------------------------------------------------

--
-- Table structure for table `kategori_konsumen`
--

CREATE TABLE IF NOT EXISTS `kategori_konsumen` (
  `id_kategori_konsumen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_konsumen` varchar(20) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id_kategori_konsumen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kategori_konsumen`
--


-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE IF NOT EXISTS `kelompok` (
  `id_kelompok` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelompok` varchar(20) DEFAULT NULL,
  `menambahkan` int(11) DEFAULT '0',
  `melihat` int(11) DEFAULT '0',
  `merubah` int(11) DEFAULT '0',
  `menghapus` int(11) DEFAULT '0',
  PRIMARY KEY (`id_kelompok`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kelompok`
--


-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE IF NOT EXISTS `keranjang` (
  `id_keranjang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` int(11) DEFAULT '0',
  `jumlah` int(11) DEFAULT '0',
  `ref_keranjang` int(11) DEFAULT '0',
  PRIMARY KEY (`id_keranjang`),
  KEY `kode_barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `keranjang`
--


-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE IF NOT EXISTS `konsumen` (
  `id_konsumen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_konsumen` varchar(40) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(12) DEFAULT NULL,
  `nama_instansi` varchar(40) DEFAULT NULL,
  `kategori_konsumen_id` int(11) DEFAULT NULL,
  `waktu_terdaftar` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_konsumen`),
  KEY `kategori_komsumen_id` (`kategori_konsumen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `konsumen`
--


-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE IF NOT EXISTS `merk` (
  `id_merk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_merk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merk`
--


-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pengguna` varchar(20) DEFAULT NULL,
  `sandi` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `kelompok` int(11) DEFAULT NULL,
  `waktu_terdaftar` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pengguna`),
  KEY `kelompok` (`kelompok`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pengguna`
--


-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
  `pengguna_id` int(11) NOT NULL,
  `nama_lengkap` varchar(40) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(12) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `waktu_terdaftar` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pengguna_id`),
  UNIQUE KEY `pengguna_id_UNIQUE` (`pengguna_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--


-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satuan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `satuan`
--


-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('81f878008d7a7bc144e9b782c2a7aee8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0', 1370370787, ''),
('b0cc59aabf2402341138a0415e73e0a3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36', 1370371027, '');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `keranjang_id` int(11) DEFAULT NULL,
  `jumlah_pembayaran` int(11) DEFAULT NULL,
  `jumlah_kembalian` int(11) DEFAULT '0',
  `komsumen_id` int(11) DEFAULT NULL,
  `jenis_transaksi` int(11) DEFAULT NULL,
  `pengirim` int(11) DEFAULT NULL,
  `pengguna_id` int(11) DEFAULT NULL,
  `waktu_transaksi` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `keranjang_id` (`keranjang_id`),
  KEY `komsumen_id` (`komsumen_id`),
  KEY `jenis_transaksi` (`jenis_transaksi`),
  KEY `pengirim` (`pengirim`),
  KEY `pengguna_id` (`pengguna_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `transaksi`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `gudang`
--
ALTER TABLE `gudang`
  ADD CONSTRAINT `gudang_ibfk_3` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id_satuan`),
  ADD CONSTRAINT `gudang_ibfk_1` FOREIGN KEY (`jenis`) REFERENCES `jenis` (`id_jenis`),
  ADD CONSTRAINT `gudang_ibfk_2` FOREIGN KEY (`merk`) REFERENCES `merk` (`id_merk`);

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_ibfk_1` FOREIGN KEY (`kode_barang`) REFERENCES `gudang` (`kode`);

--
-- Constraints for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD CONSTRAINT `konsumen_ibfk_1` FOREIGN KEY (`kategori_konsumen_id`) REFERENCES `kategori_konsumen` (`id_kategori_konsumen`);

--
-- Constraints for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`kelompok`) REFERENCES `kelompok` (`id_kelompok`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_5` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`id_pengguna`),
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`keranjang_id`) REFERENCES `keranjang` (`id_keranjang`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`komsumen_id`) REFERENCES `konsumen` (`id_konsumen`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`jenis_transaksi`) REFERENCES `jenis_transaksi` (`id_jenis_transaksi`),
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`pengirim`) REFERENCES `pengguna` (`id_pengguna`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
